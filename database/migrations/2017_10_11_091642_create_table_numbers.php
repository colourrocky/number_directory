<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNumbers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number', 50);
            $table->string('arabic_number', 50)->nullable();
            $table->string('name', 50);
            $table->string('arabic_name', 50)->nullable();
            $table->integer('category_id');
            $table->integer('arabic_category_id')->nullable();
            $table->integer('area_id');
            $table->integer('arabic_area_id')->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numbers');
    }
}
