@extends('layouts.app')

@section('content')<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Number</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('numbers') }}/{{ $number->id }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                            <label for="number" class="col-md-4 control-label">Number in English</label>

                            <div class="col-md-6">
                                <input id="number" type="text" class="form-control" name="number" value="{{ $number->number }}" required autofocus>

                                @if ($errors->has('number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('arabic_number') ? ' has-error' : '' }}">
                            <label for="arabic_number" class="col-md-4 control-label">العدد باللغة العربية</label>

                            <div class="col-md-6">
                                <input id="arabic_number" type="text" class="form-control" name="arabic_number" value="{{ $number->arabic_number }}" required autofocus>

                                @if ($errors->has('arabic_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('arabic_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $number->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('arabic_name') ? ' has-error' : '' }}">
                            <label for="arabic_name" class="col-md-4 control-label">اسم</label>

                            <div class="col-md-6">
                                <input id="arabic_name" type="text" class="form-control" name="arabic_name" value="{{ $number->arabic_name }}" required autofocus>

                                @if ($errors->has('arabic_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('arabic_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="category_id" class="col-md-4 control-label">Select Category</label>

                            <div class="col-md-6">
                                <select name="category_id" id="category_id" class="form-control">
                                  <option value="0" >--Select Category--</option>
                                  @if(!empty($all_categories))
                                    @foreach($all_categories as $category)
                                      <option 
                                        @if($category->id == $number->category_id)
                                            selected="selected"
                                        @endif
                                      value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                  @endif  
                                </select>

                                @if ($errors->has('category_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('arabic_category_id') ? ' has-error' : '' }}">
                            <label for="arabic_category_id" class="col-md-4 control-label">اختر الفئة</label>

                            <div class="col-md-6">
                                <select name="arabic_category_id" id="arabic_category_id" class="form-control">
                                  <option value="0" >--اختر الفئة--</option>
                                  @if(!empty($all_categories))
                                    @foreach($all_categories as $category)
                                      <option 
                                        @if($category->id == $number->arabic_category_id)
                                            selected="selected"
                                        @endif
                                      value="{{ $category->id }}">{{ $category->arabic_name }}</option>
                                    @endforeach
                                  @endif  
                                </select>

                                @if ($errors->has('arabic_category_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('arabic_category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('area_id') ? ' has-error' : '' }}">
                            <label for="area_id" class="col-md-4 control-label">Select Area</label>

                            <div class="col-md-6">
                                <select name="area_id" id="area_id" class="form-control">
                                  <option value="0" >--Select Area--</option>
                                  @if(!empty($all_areas))
                                    @foreach($all_areas as $area)
                                      <option 
                                            @if($area->id == $number->area_id)
                                                selected="selected"
                                            @endif
                                      value="{{ $area->id }}">{{ $area->name }}</option>
                                    @endforeach
                                  @endif  
                                </select>

                                @if ($errors->has('area_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('area_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('arabic_area_id') ? ' has-error' : '' }}">
                            <label for="arabic_area_id" class="col-md-4 control-label">حدد المنطقة</label>

                            <div class="col-md-6">
                                <select name="arabic_area_id" id="arabic_area_id" class="form-control">
                                  <option value="0" >--حدد المنطقة--</option>
                                  @if(!empty($all_areas))
                                    @foreach($all_areas as $area)
                                      <option 
                                            @if($area->id == $number->arabic_area_id)
                                                selected="selected"
                                            @endif
                                      value="{{ $area->id }}">{{ $area->arabic_name }}</option>
                                    @endforeach
                                  @endif  
                                </select>

                                @if ($errors->has('arabic_area_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('arabic_area_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection