@extends('layouts.app')

@section('content')<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Import Numbers From csv</div>

                <div class="panel-body">
                    <form method="POST" action="{{ url('importfileintodb') }}" enctype="multipart/form-data">

                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('number_file') ? ' has-error' : '' }}">
                            <label for="number_file" class="col-md-4 control-label">Select CSV File</label>

                            <div class="col-md-6">
                                <input id="number_file" type="file" class="" name="number_file" value="" required autofocus>

                            </div>
                        </div>

                        <input type="submit" name="" value="Submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection