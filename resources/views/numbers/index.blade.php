@extends('layouts.app')

@section('content')
	
	<div class="container">
    <div class="row">
		
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">

                <div class="panel-heading">
                	Areas
                	<a class="btn btn-primary btn-sm" href="{{ url('numbers/create')}}">Add New</a>
                  
                  <a href="{{ url('upload') }}" class="btn btn-success btn-sm">Upload CSV <i class="fa fa-upload" aria-hidden="true"></i></a>
                 

                  <span class="pull-right">Total Numbers : {{ count($all_numbers) }}</span>
                  <!--<span>Total Non Confirmed Numbers  : </span>-->
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Number</th>
                  <th>رقم</th>
                  <th>Name</th>
                  <th>اسم</th>
                  <th>Category</th>
                  <th>الفئة</th>
                  <th>Area</th>
                  <th>منطقة</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody><?php $i = 0 ?>
              	@foreach($all_numbers as $number)
                <tr>
                  <td>{{++$i}}</td>

                  <td><a href="{{ url('numbers')}}/{{ $number->id }}/edit">{{ $number->number }}</a></td>
                  <td><a href="{{ url('numbers')}}/{{ $number->id }}/edit">
                    @if(!empty($number->arabic_number))
                      {{ $number->arabic_number }}
                    @endif
                  </a></td>

                  <td>{{ $number->name }}</td>
                  <td>
                    @if(!empty($number->arabic_name))
                      {{ $number->arabic_name }}
                    @endif
                  </td>

                  <td>
                    {{ $category = App\Category::find($number->category_id)->name }}
                  </td>
                  <td>
                    @if(!empty($number->arabic_category_id))
                    {{ $category = App\Category::find($number->arabic_category_id)->arabic_name }}
                    @endif
                  </td>

                  <td>{{ $area = App\Area::find($number->area_id)->name }}</td>

                  <td>
                    @if(!empty($number->arabic_area_id))
                    {{ $area = App\Area::find($number->arabic_area_id)->arabic_name }}
                    @endif
                  </td>

                  <td>
                    @if($number->status == '1')
                    <span class="btn btn-primary btn-sm">Confirmed</span>

                    @else
                    <a href="{{ url('makeconfirm') }}/{{ $number->id }}" class="btn btn-danger btn-sm" title="Click to confirm" data-toggle="tooltip" data-placement="bottom">
                      
                        Not Confirmed
                      
                    </a>
                    
                    @endif
                      
                </td>
					<td>
					<form method="POST" action="{{ url('/numbers')}}/{{ $number->id }}">

						{{ csrf_field() }}
						{{ method_field('DELETE') }}

							<button type="submit" class="btn btn-danger btn-sm">Delete</button>
						
						</form>	


					</td>
          <td>
            <a class="btn btn-primary btn-sm" href="{{ url('numbers')}}/{{ $number->id }}/edit">Edit</a>
          </td>
                </tr>
                @endforeach
                
              </tbody>
            </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection