@extends('layouts.app')

@section('content')<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="padding: 10px;">
                    Search Result
                    <!--<a href="{{ url('addnumber') }}" class="btn btn-primary btn-sm pull-right">Add Number</a>-->
                </div>
                
                <div class="panel-body">
                    @if(isset($number->number))
                        <b>Number :</b> {{ $number->number }} <br><br>
                        <b>Name :</b> {{ $number->name }}
                        <br><br>
                        <b>Category :</b> {{ App\Category::find($number->category_id)->name }}<br><br>
                        <b>Area :</b> {{ App\Area::find($number->area_id)->name }}<br><br>

                    @else

                        <b>رقم :</b> {{ $number->arabic_number }} <br><br>
                        <b>اسم :</b> {{ $number->arabic_name }}
                        <br><br>
                        <b>الفئة :</b> {{ App\Category::find($number->arabic_category_id)->arabic_name }}<br><br>
                        <b>منطقة :</b> {{ App\Area::find($number->arabic_area_id)->arabic_name }}<br><br>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection