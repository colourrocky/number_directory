@extends('layouts.app')

@section('content')
	
	<div class="container">
    <div class="row">
		
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">

                <div class="panel-heading">
                	Categories
                	<a class="btn btn-primary" href="{{ url('/categories/create') }}">Add New</a>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>اسم</th>
                  <!--<th>Description</th>
                  <th>وصف</th>-->
                </tr>
              </thead>
              <tbody>
              	@foreach($all_categories as $cat)
                <tr>
                  <td></td>
                  <td><a href="{{ url('/categories')}}/{{ $cat->id }}/edit">{{ $cat->name }}</a></td>
                  <td><a href="{{ url('/categories')}}/{{ $cat->id }}/edit">{{ $cat->arabic_name }}</a></td>
					<td>
					<form method="POST" action="{{ url('/categories')}}/{{ $cat->id }}">

						{{ csrf_field() }}
						{{ method_field('DELETE') }}

							<button type="submit" class="btn btn-danger btn-sm">Delete</button>
						
						</form>	
					</td>
                </tr>
                @endforeach
                
              </tbody>
            </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection