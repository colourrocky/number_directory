@extends('layouts.app')

@section('content')<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Area</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('areas') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('arabic_name') ? ' has-error' : '' }}">
                            <label for="arabic_name" class="col-md-4 control-label">اسم</label>

                            <div class="col-md-6">
                                <input id="arabic_name" type="text" class="form-control" name="arabic_name" value="{{ old('arabic_name') }}" required autofocus>

                                @if ($errors->has('arabic_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('arabic_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <!--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">
                                <textarea id="description" name="description" class="form-control"></textarea>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('arabic_description') ? ' has-error' : '' }}">
                            <label for="arabic_description" class="col-md-4 control-label">وصف</label>

                            <div class="col-md-6">
                                <textarea id="arabic_description" name="arabic_description" class="form-control"></textarea>

                                @if ($errors->has('arabic_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('arabic_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>-->

                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection