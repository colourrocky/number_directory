<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Number;

class Select2AutocompleteController extends Controller
{
    public function dataAjax(Request $request)
    {
    	$data = [];

        if($request->has('q')){
            $search = $request->q;
        }

        $data = DB::table("numbers")
                    ->select("name as id","name")
                    ->where('name','LIKE',"%$search%")
                    ->where('status', '=', 1)
                    //->orWhere('arabic_name', 'LIKE', "%$search%")
                    ->get();

        if(sizeof($data)  == 0){
            
            $data = DB::table("numbers")
                    ->select("arabic_name as id","arabic_name as name")
                    ->where('arabic_name','LIKE',"%$search%")
                    ->where('status', '=', 1)
                    //->orWhere('arabic_name', 'LIKE', "%$search%")
                    ->get();
                    return json_encode($data, JSON_UNESCAPED_UNICODE);
        }else{
            return response()->json($data);
        }
        
        
    }

     public function search(Request $request){
        $data = $request->all();
        $id = $data['itemName'];
       //die($id);
        $number = DB::table('numbers')
        ->select('number', 'name', 'category_id', 'area_id')
        ->where('name', $id)->get()->first();

        //print_r($number);
        
        if(empty($number)){

            $number = DB::table('numbers')
            ->select('arabic_number', 'arabic_name', 'arabic_category_id', 'arabic_area_id')
            ->where('arabic_name', $id)->get()->first();
        }
        return view('numbers.searchresult', compact('number'));
    }

    public function createNumberByUser(){
    	 $all_categories = DB::table('categories')->get();
        $all_areas = DB::table('areas')->get();
    	return view('numbers.user_create', compact(['all_categories', 'all_areas']));
    }

    public function storeNumBerbyUser(Request $request){
        $number = new Number;
        $number->number = $request->number;
        //$number->number_arabic = $request->number_arabic;
        $number->name = $request->name;
        $number->category_id = $request->category_id;
        $number->area_id = $request->area_id;
        $number->status = '0';
        
        $number->save();
        return redirect('/thankyou');
    }

    public function thankYouPage(){
        return view('numbers.thankyou');
    }

}
