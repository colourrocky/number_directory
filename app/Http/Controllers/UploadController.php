<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UploadRequest;

class UploadController extends Controller
{
    
    public function uploadSubmit(UploadRequest $request){
    	$thumb = $request->number_file;
        $file_name = $thumb->store('numbers');
    }
}
