<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Number;
use DB;

class NumbersController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        $all_numbers = DB::table('numbers')->get()->toArray();
        
        return view('numbers.index', compact('all_numbers'));
    }

   
    public function create()
    {
        $all_categories = DB::table('categories')->get();
        $all_areas = DB::table('areas')->get();

        return view('numbers.create', compact(['all_categories', 'all_areas']));
    }

    
    public function store(Request $request)
    {
        $number = new Number;
        $number->number = $request->number;
        $number->arabic_number = $request->arabic_number;

        $number->name = $request->name;
        $number->arabic_name = $request->arabic_name;

        $number->category_id = $request->category_id;
        $number->arabic_category_id = $request->arabic_category_id;

        $number->area_id = $request->area_id;
        $number->arabic_area_id = $request->arabic_area_id;

        $number->save();

        return redirect('/numbers');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $selected = '';
        $all_categories = DB::table('categories')->get();
        $all_areas = DB::table('areas')->get();
        $number = DB::table('numbers')->where('id', $id)->get()->first();

        return view("numbers.edit", compact(['number', 'all_categories', 'all_areas']));
    }

    
    public function update(Request $request, $id)
    {
        
        $number = array();
        $number['number'] = $request->number;
        $number['arabic_number'] = $request->arabic_number;

        $number['name'] = $request->name;
        $number['arabic_name'] = $request->arabic_name;

        $number['category_id'] = $request->category_id;
        $number['arabic_category_id'] = $request->arabic_category_id;

        $number['area_id'] = $request->area_id;
        $number['arabic_area_id'] = $request->arabic_area_id;

        DB::table('numbers')
            ->where('id', $id)
            ->update($number);

        return redirect('/numbers');
    }

    
    public function destroy($id)
    {
        DB::table('numbers')->where('id', $id)->delete();
        return redirect(url("/numbers"));
    }

    public function makeconfirm($id){
        DB::table('numbers')->where('id', $id)->update(['status' => '1']);

        return redirect(url("/numbers"));
    }

   
}
