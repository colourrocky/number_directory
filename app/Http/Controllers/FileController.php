<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class FileController extends Controller
{
	public function __construct(){
        $this->middleware('auth');
    }
    public function importFileIntoDB(Request $request){
    	 

    	if($request->hasFile('number_file')){
            $path = $request->file('number_file')->getRealPath();
            $data = \Excel::load($path)->get();
            if($data->count()){
                foreach ($data as $key => $value) {
                	// check number already exist or not
                	$number = DB::table('numbers')->pluck('number')->toArray();

				    if(in_array($value->number, $number)){
				    	continue;
				    }


				    $category_id = $this->checkCategory($value->category, $value->arabic_category);

                    $area_id = $this->checkArea($value->area, $value->arabic_area);
			    	
				    

                    $arr[] = 
                    [
                    	'number' => $value->number, 
                    	'arabic_number' => $value->arabic_number,
                    	'name' => $value->name,
                    	'arabic_name' => $value->arabic_name,
                    	'category_id' => $category_id,
                    	'arabic_category_id' => $category_id,
                    	'area_id' => $area_id,
                    	'arabic_area_id' => $area_id,
                	];
                }
                //print_r($arr); die();
                if(!empty($arr)){

                    DB::table('numbers')->insert($arr);

                    //dd('Insert Record successfully.');
                    return redirect('/numbers');
                }
            }
        }
        dd('Request data does not have any files to import.');  

    }

    public function uploadCSV(){
    	return view('numbers.upload');
    }

    public function checkCategory($excel_category, $excel_arabic_category){
        $category_id = '';
    	// if not exist in db
    	// check for category
    	$category = DB::table('categories')->where('name', $excel_category)->get()->first();

        if(!empty($category)){ 
            $category_id = $category->id;
        } 
        else{
    		// isert new category
    		$cat = array();
    		$cat['name'] = $excel_category;
            $cat['arabic_name'] = $excel_arabic_category;

    		$category_id = DB::table('categories')->insertGetId($cat);
        }


        return $category_id;
    }

    public function checkArea($excel_area, $excel_arabic_area){
        // if not exist in db
        // check for area
        $area = DB::table('areas')->where('name', $excel_area)->get()->first();

        if(!empty($area)){
            $area_id = $area->id;
        }else{
            // isert new category
            $area = array();
            $area['name'] = $excel_area;
            $area['arabic_name'] = $excel_arabic_area;

            $area_id = DB::table('areas')->insertGetId($area);
        }

        return $area_id;
    }
}
