<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use DB;

class AreaController extends Controller
{
     public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        $all_areas = DB::table('areas')->get();
        return view('areas.index', compact('all_areas'));
    }

   
    public function create()
    {
        //$all_areas = DB::table('areas')->where('parent_id', 0)->get();
        return view('areas.create');
    }

    
    public function store(Request $request)
    {
        $area = new Area;
        $area->name = $request->name;
        $area->arabic_name = $request->arabic_name;
       

        $area->save();

        return redirect('/areas');
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $selected = '';
        $area = DB::table('areas')->where('id', $id)->get()->first();
        return view("areas.edit", compact('area'));
    }

    
    public function update(Request $request, $id)
    {
        
        $area = array();
        $area['name'] = $request->name;
        $area['arabic_name'] = $request->arabic_name;
       
       
        DB::table('areas')
            ->where('id', $id)
            ->update($area);

        return redirect('/areas');
    }

    
    public function destroy($id)
    {
        DB::table('areas')->where('id', $id)->delete();
        return redirect(url("/areas"));
    }
}
