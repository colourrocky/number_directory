<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('categories', 'CategoryController');
Route::resource('areas', 'AreaController');


Route::resource('numbers', 'NumbersController');
Route::get('makeconfirm/{id}', 'NumbersController@makeconfirm');

Route::get('select2-autocomplete-ajax', 'Select2AutocompleteController@dataAjax');

Route::post('searchresult', 'Select2AutocompleteController@search');

Route::get('addnumber', 'Select2AutocompleteController@createNumberByUser');



Route::get('thankyou', 'Select2AutocompleteController@thankYouPage');

Route::post('usercreatenumber', 'Select2AutocompleteController@storeNumBerbyUser');

Route::get('upload', 'FileController@uploadCSV');
Route::post('importfileintodb', 'FileController@importFileIntoDB');


